﻿namespace MobiFlight
{
    partial class SettingsDialog
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsDialog));
            this.panel1 = new System.Windows.Forms.Panel();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.ledDisplaysTabPage = new System.Windows.Forms.TabPage();
            this.arcazeModuleSettingsGroupBox = new System.Windows.Forms.GroupBox();
            this.numModulesLabel = new System.Windows.Forms.Label();
            this.numModulesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.arcazeModuleTypeComboBox = new System.Windows.Forms.ComboBox();
            this.globalBrightnessLabel = new System.Windows.Forms.Label();
            this.globalBrightnessTrackBar = new System.Windows.Forms.TrackBar();
            this.arcazeModuleTypeLabel = new System.Windows.Forms.Label();
            this.arcazeModulesGroupBox = new System.Windows.Forms.GroupBox();
            this.ArcazeModuleTreeView = new System.Windows.Forms.TreeView();
            this.mfTreeViewImageList = new System.Windows.Forms.ImageList(this.components);
            this.arcazeSettingsLabel = new System.Windows.Forms.Label();
            this.mfModuleSettingsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ledOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ledSegmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stepperToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LcdDisplayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.encoderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.uploadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.updateFirmwareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regenerateSerialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalTabPage = new System.Windows.Forms.TabPage();
            this.offlineModeGroupBox = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.offlineModeCheckBox = new System.Windows.Forms.CheckBox();
            this.debugGroupBox = new System.Windows.Forms.GroupBox();
            this.logLevelComboBox = new System.Windows.Forms.ComboBox();
            this.logLevelLabel = new System.Windows.Forms.Label();
            this.logLevelCheckBox = new System.Windows.Forms.CheckBox();
            this.testModeSpeedGroupBox = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.testModeSpeedTrackBar = new System.Windows.Forms.TrackBar();
            this.recentFilesGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.recentFilesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.mobiFlightTabPage = new System.Windows.Forms.TabPage();
            this.mfConfiguredModulesGroupBox = new System.Windows.Forms.GroupBox();
            this.mfModulesTreeView = new System.Windows.Forms.TreeView();
            this.mfSettingsPanel = new System.Windows.Forms.Panel();
            this.mobiflightSettingsToolStrip = new System.Windows.Forms.ToolStrip();
            this.uploadToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.addDeviceToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.addEncoderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addButtonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.addStepperToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addServoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addLedModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addLcdDisplayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDeviceToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.firmwareSettingsGroupBox = new System.Windows.Forms.GroupBox();
            this.FwAutoUpdateCheckBox = new System.Windows.Forms.CheckBox();
            this.firmwareArduinoIdeButton = new System.Windows.Forms.Button();
            this.firmwareArduinoIdePathTextBox = new System.Windows.Forms.TextBox();
            this.firmwareArduinoIdeLabel = new System.Windows.Forms.Label();
            this.mobiflightSettingsLabel = new System.Windows.Forms.Label();
            this.fsuipcTabPage = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.fsuipcPollIntervalTrackBar = new System.Windows.Forms.TrackBar();
            this.firmwareSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.firmwareUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.firmwareUpdateBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            this.ledDisplaysTabPage.SuspendLayout();
            this.arcazeModuleSettingsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numModulesNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.globalBrightnessTrackBar)).BeginInit();
            this.arcazeModulesGroupBox.SuspendLayout();
            this.mfModuleSettingsContextMenuStrip.SuspendLayout();
            this.generalTabPage.SuspendLayout();
            this.offlineModeGroupBox.SuspendLayout();
            this.debugGroupBox.SuspendLayout();
            this.testModeSpeedGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testModeSpeedTrackBar)).BeginInit();
            this.recentFilesGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recentFilesNumericUpDown)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.mobiFlightTabPage.SuspendLayout();
            this.mfConfiguredModulesGroupBox.SuspendLayout();
            this.mobiflightSettingsToolStrip.SuspendLayout();
            this.firmwareSettingsGroupBox.SuspendLayout();
            this.fsuipcTabPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fsuipcPollIntervalTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Controls.Add(this.cancelButton);
            this.errorProvider1.SetError(this.panel1, resources.GetString("panel1.Error"));
            this.errorProvider1.SetIconAlignment(this.panel1, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("panel1.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.panel1, ((int)(resources.GetObject("panel1.IconPadding"))));
            this.panel1.Name = "panel1";
            this.toolTip1.SetToolTip(this.panel1, resources.GetString("panel1.ToolTip"));
            // 
            // okButton
            // 
            resources.ApplyResources(this.okButton, "okButton");
            this.errorProvider1.SetError(this.okButton, resources.GetString("okButton.Error"));
            this.errorProvider1.SetIconAlignment(this.okButton, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("okButton.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.okButton, ((int)(resources.GetObject("okButton.IconPadding"))));
            this.okButton.Name = "okButton";
            this.toolTip1.SetToolTip(this.okButton, resources.GetString("okButton.ToolTip"));
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            resources.ApplyResources(this.cancelButton, "cancelButton");
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.errorProvider1.SetError(this.cancelButton, resources.GetString("cancelButton.Error"));
            this.errorProvider1.SetIconAlignment(this.cancelButton, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("cancelButton.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.cancelButton, ((int)(resources.GetObject("cancelButton.IconPadding"))));
            this.cancelButton.Name = "cancelButton";
            this.toolTip1.SetToolTip(this.cancelButton, resources.GetString("cancelButton.ToolTip"));
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // ledDisplaysTabPage
            // 
            resources.ApplyResources(this.ledDisplaysTabPage, "ledDisplaysTabPage");
            this.ledDisplaysTabPage.Controls.Add(this.arcazeModuleSettingsGroupBox);
            this.ledDisplaysTabPage.Controls.Add(this.arcazeModulesGroupBox);
            this.ledDisplaysTabPage.Controls.Add(this.arcazeSettingsLabel);
            this.errorProvider1.SetError(this.ledDisplaysTabPage, resources.GetString("ledDisplaysTabPage.Error"));
            this.errorProvider1.SetIconAlignment(this.ledDisplaysTabPage, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("ledDisplaysTabPage.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.ledDisplaysTabPage, ((int)(resources.GetObject("ledDisplaysTabPage.IconPadding"))));
            this.ledDisplaysTabPage.Name = "ledDisplaysTabPage";
            this.toolTip1.SetToolTip(this.ledDisplaysTabPage, resources.GetString("ledDisplaysTabPage.ToolTip"));
            this.ledDisplaysTabPage.UseVisualStyleBackColor = true;
            this.ledDisplaysTabPage.Validating += new System.ComponentModel.CancelEventHandler(this.ledDisplaysTabPage_Validating);
            // 
            // arcazeModuleSettingsGroupBox
            // 
            resources.ApplyResources(this.arcazeModuleSettingsGroupBox, "arcazeModuleSettingsGroupBox");
            this.arcazeModuleSettingsGroupBox.Controls.Add(this.numModulesLabel);
            this.arcazeModuleSettingsGroupBox.Controls.Add(this.numModulesNumericUpDown);
            this.arcazeModuleSettingsGroupBox.Controls.Add(this.arcazeModuleTypeComboBox);
            this.arcazeModuleSettingsGroupBox.Controls.Add(this.globalBrightnessLabel);
            this.arcazeModuleSettingsGroupBox.Controls.Add(this.globalBrightnessTrackBar);
            this.arcazeModuleSettingsGroupBox.Controls.Add(this.arcazeModuleTypeLabel);
            this.errorProvider1.SetError(this.arcazeModuleSettingsGroupBox, resources.GetString("arcazeModuleSettingsGroupBox.Error"));
            this.errorProvider1.SetIconAlignment(this.arcazeModuleSettingsGroupBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("arcazeModuleSettingsGroupBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.arcazeModuleSettingsGroupBox, ((int)(resources.GetObject("arcazeModuleSettingsGroupBox.IconPadding"))));
            this.arcazeModuleSettingsGroupBox.Name = "arcazeModuleSettingsGroupBox";
            this.arcazeModuleSettingsGroupBox.TabStop = false;
            this.toolTip1.SetToolTip(this.arcazeModuleSettingsGroupBox, resources.GetString("arcazeModuleSettingsGroupBox.ToolTip"));
            // 
            // numModulesLabel
            // 
            resources.ApplyResources(this.numModulesLabel, "numModulesLabel");
            this.errorProvider1.SetError(this.numModulesLabel, resources.GetString("numModulesLabel.Error"));
            this.errorProvider1.SetIconAlignment(this.numModulesLabel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("numModulesLabel.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.numModulesLabel, ((int)(resources.GetObject("numModulesLabel.IconPadding"))));
            this.numModulesLabel.Name = "numModulesLabel";
            this.toolTip1.SetToolTip(this.numModulesLabel, resources.GetString("numModulesLabel.ToolTip"));
            // 
            // numModulesNumericUpDown
            // 
            resources.ApplyResources(this.numModulesNumericUpDown, "numModulesNumericUpDown");
            this.errorProvider1.SetError(this.numModulesNumericUpDown, resources.GetString("numModulesNumericUpDown.Error"));
            this.errorProvider1.SetIconAlignment(this.numModulesNumericUpDown, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("numModulesNumericUpDown.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.numModulesNumericUpDown, ((int)(resources.GetObject("numModulesNumericUpDown.IconPadding"))));
            this.numModulesNumericUpDown.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numModulesNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numModulesNumericUpDown.Name = "numModulesNumericUpDown";
            this.toolTip1.SetToolTip(this.numModulesNumericUpDown, resources.GetString("numModulesNumericUpDown.ToolTip"));
            this.numModulesNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numModulesNumericUpDown.ValueChanged += new System.EventHandler(this.numModulesNumericUpDown_ValueChanged);
            // 
            // arcazeModuleTypeComboBox
            // 
            resources.ApplyResources(this.arcazeModuleTypeComboBox, "arcazeModuleTypeComboBox");
            this.arcazeModuleTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.errorProvider1.SetError(this.arcazeModuleTypeComboBox, resources.GetString("arcazeModuleTypeComboBox.Error"));
            this.arcazeModuleTypeComboBox.FormattingEnabled = true;
            this.errorProvider1.SetIconAlignment(this.arcazeModuleTypeComboBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("arcazeModuleTypeComboBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.arcazeModuleTypeComboBox, ((int)(resources.GetObject("arcazeModuleTypeComboBox.IconPadding"))));
            this.arcazeModuleTypeComboBox.Items.AddRange(new object[] {
            resources.GetString("arcazeModuleTypeComboBox.Items")});
            this.arcazeModuleTypeComboBox.Name = "arcazeModuleTypeComboBox";
            this.toolTip1.SetToolTip(this.arcazeModuleTypeComboBox, resources.GetString("arcazeModuleTypeComboBox.ToolTip"));
            this.arcazeModuleTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.arcazeModuleTypeComboBox_SelectedIndexChanged);
            // 
            // globalBrightnessLabel
            // 
            resources.ApplyResources(this.globalBrightnessLabel, "globalBrightnessLabel");
            this.errorProvider1.SetError(this.globalBrightnessLabel, resources.GetString("globalBrightnessLabel.Error"));
            this.errorProvider1.SetIconAlignment(this.globalBrightnessLabel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("globalBrightnessLabel.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.globalBrightnessLabel, ((int)(resources.GetObject("globalBrightnessLabel.IconPadding"))));
            this.globalBrightnessLabel.Name = "globalBrightnessLabel";
            this.toolTip1.SetToolTip(this.globalBrightnessLabel, resources.GetString("globalBrightnessLabel.ToolTip"));
            // 
            // globalBrightnessTrackBar
            // 
            resources.ApplyResources(this.globalBrightnessTrackBar, "globalBrightnessTrackBar");
            this.globalBrightnessTrackBar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.errorProvider1.SetError(this.globalBrightnessTrackBar, resources.GetString("globalBrightnessTrackBar.Error"));
            this.errorProvider1.SetIconAlignment(this.globalBrightnessTrackBar, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("globalBrightnessTrackBar.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.globalBrightnessTrackBar, ((int)(resources.GetObject("globalBrightnessTrackBar.IconPadding"))));
            this.globalBrightnessTrackBar.Maximum = 9;
            this.globalBrightnessTrackBar.Minimum = 1;
            this.globalBrightnessTrackBar.Name = "globalBrightnessTrackBar";
            this.toolTip1.SetToolTip(this.globalBrightnessTrackBar, resources.GetString("globalBrightnessTrackBar.ToolTip"));
            this.globalBrightnessTrackBar.Value = 9;
            this.globalBrightnessTrackBar.ValueChanged += new System.EventHandler(this.numModulesNumericUpDown_ValueChanged);
            // 
            // arcazeModuleTypeLabel
            // 
            resources.ApplyResources(this.arcazeModuleTypeLabel, "arcazeModuleTypeLabel");
            this.errorProvider1.SetError(this.arcazeModuleTypeLabel, resources.GetString("arcazeModuleTypeLabel.Error"));
            this.errorProvider1.SetIconAlignment(this.arcazeModuleTypeLabel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("arcazeModuleTypeLabel.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.arcazeModuleTypeLabel, ((int)(resources.GetObject("arcazeModuleTypeLabel.IconPadding"))));
            this.arcazeModuleTypeLabel.Name = "arcazeModuleTypeLabel";
            this.toolTip1.SetToolTip(this.arcazeModuleTypeLabel, resources.GetString("arcazeModuleTypeLabel.ToolTip"));
            // 
            // arcazeModulesGroupBox
            // 
            resources.ApplyResources(this.arcazeModulesGroupBox, "arcazeModulesGroupBox");
            this.arcazeModulesGroupBox.Controls.Add(this.ArcazeModuleTreeView);
            this.errorProvider1.SetError(this.arcazeModulesGroupBox, resources.GetString("arcazeModulesGroupBox.Error"));
            this.errorProvider1.SetIconAlignment(this.arcazeModulesGroupBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("arcazeModulesGroupBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.arcazeModulesGroupBox, ((int)(resources.GetObject("arcazeModulesGroupBox.IconPadding"))));
            this.arcazeModulesGroupBox.Name = "arcazeModulesGroupBox";
            this.arcazeModulesGroupBox.TabStop = false;
            this.toolTip1.SetToolTip(this.arcazeModulesGroupBox, resources.GetString("arcazeModulesGroupBox.ToolTip"));
            // 
            // ArcazeModuleTreeView
            // 
            resources.ApplyResources(this.ArcazeModuleTreeView, "ArcazeModuleTreeView");
            this.errorProvider1.SetError(this.ArcazeModuleTreeView, resources.GetString("ArcazeModuleTreeView.Error"));
            this.errorProvider1.SetIconAlignment(this.ArcazeModuleTreeView, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("ArcazeModuleTreeView.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.ArcazeModuleTreeView, ((int)(resources.GetObject("ArcazeModuleTreeView.IconPadding"))));
            this.ArcazeModuleTreeView.ImageList = this.mfTreeViewImageList;
            this.ArcazeModuleTreeView.Name = "ArcazeModuleTreeView";
            this.ArcazeModuleTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            ((System.Windows.Forms.TreeNode)(resources.GetObject("ArcazeModuleTreeView.Nodes"))),
            ((System.Windows.Forms.TreeNode)(resources.GetObject("ArcazeModuleTreeView.Nodes1")))});
            this.toolTip1.SetToolTip(this.ArcazeModuleTreeView, resources.GetString("ArcazeModuleTreeView.ToolTip"));
            this.ArcazeModuleTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.ArcazeModuleTreeView_AfterSelect);
            // 
            // mfTreeViewImageList
            // 
            this.mfTreeViewImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            resources.ApplyResources(this.mfTreeViewImageList, "mfTreeViewImageList");
            this.mfTreeViewImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // arcazeSettingsLabel
            // 
            resources.ApplyResources(this.arcazeSettingsLabel, "arcazeSettingsLabel");
            this.errorProvider1.SetError(this.arcazeSettingsLabel, resources.GetString("arcazeSettingsLabel.Error"));
            this.errorProvider1.SetIconAlignment(this.arcazeSettingsLabel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("arcazeSettingsLabel.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.arcazeSettingsLabel, ((int)(resources.GetObject("arcazeSettingsLabel.IconPadding"))));
            this.arcazeSettingsLabel.Name = "arcazeSettingsLabel";
            this.toolTip1.SetToolTip(this.arcazeSettingsLabel, resources.GetString("arcazeSettingsLabel.ToolTip"));
            // 
            // mfModuleSettingsContextMenuStrip
            // 
            resources.ApplyResources(this.mfModuleSettingsContextMenuStrip, "mfModuleSettingsContextMenuStrip");
            this.errorProvider1.SetError(this.mfModuleSettingsContextMenuStrip, resources.GetString("mfModuleSettingsContextMenuStrip.Error"));
            this.errorProvider1.SetIconAlignment(this.mfModuleSettingsContextMenuStrip, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("mfModuleSettingsContextMenuStrip.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.mfModuleSettingsContextMenuStrip, ((int)(resources.GetObject("mfModuleSettingsContextMenuStrip.IconPadding"))));
            this.mfModuleSettingsContextMenuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mfModuleSettingsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.removeToolStripMenuItem,
            this.toolStripMenuItem1,
            this.uploadToolStripMenuItem,
            this.toolStripMenuItem2,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripMenuItem3,
            this.updateFirmwareToolStripMenuItem,
            this.regenerateSerialToolStripMenuItem,
            this.reloadConfigToolStripMenuItem});
            this.mfModuleSettingsContextMenuStrip.Name = "mfModuleSettingsContextMenuStrip";
            this.toolTip1.SetToolTip(this.mfModuleSettingsContextMenuStrip, resources.GetString("mfModuleSettingsContextMenuStrip.ToolTip"));
            this.mfModuleSettingsContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.mfModuleSettingsContextMenuStrip_Opening);
            // 
            // addToolStripMenuItem
            // 
            resources.ApplyResources(this.addToolStripMenuItem, "addToolStripMenuItem");
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ledOutputToolStripMenuItem,
            this.ledSegmentToolStripMenuItem,
            this.servoToolStripMenuItem,
            this.stepperToolStripMenuItem,
            this.LcdDisplayToolStripMenuItem,
            this.toolStripMenuItem4,
            this.buttonToolStripMenuItem,
            this.encoderToolStripMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            // 
            // ledOutputToolStripMenuItem
            // 
            resources.ApplyResources(this.ledOutputToolStripMenuItem, "ledOutputToolStripMenuItem");
            this.ledOutputToolStripMenuItem.Name = "ledOutputToolStripMenuItem";
            this.ledOutputToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // ledSegmentToolStripMenuItem
            // 
            resources.ApplyResources(this.ledSegmentToolStripMenuItem, "ledSegmentToolStripMenuItem");
            this.ledSegmentToolStripMenuItem.Name = "ledSegmentToolStripMenuItem";
            this.ledSegmentToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // servoToolStripMenuItem
            // 
            resources.ApplyResources(this.servoToolStripMenuItem, "servoToolStripMenuItem");
            this.servoToolStripMenuItem.Name = "servoToolStripMenuItem";
            this.servoToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // stepperToolStripMenuItem
            // 
            resources.ApplyResources(this.stepperToolStripMenuItem, "stepperToolStripMenuItem");
            this.stepperToolStripMenuItem.Name = "stepperToolStripMenuItem";
            this.stepperToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // LcdDisplayToolStripMenuItem
            // 
            resources.ApplyResources(this.LcdDisplayToolStripMenuItem, "LcdDisplayToolStripMenuItem");
            this.LcdDisplayToolStripMenuItem.Name = "LcdDisplayToolStripMenuItem";
            this.LcdDisplayToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            // 
            // buttonToolStripMenuItem
            // 
            resources.ApplyResources(this.buttonToolStripMenuItem, "buttonToolStripMenuItem");
            this.buttonToolStripMenuItem.Name = "buttonToolStripMenuItem";
            this.buttonToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // encoderToolStripMenuItem
            // 
            resources.ApplyResources(this.encoderToolStripMenuItem, "encoderToolStripMenuItem");
            this.encoderToolStripMenuItem.Name = "encoderToolStripMenuItem";
            this.encoderToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            resources.ApplyResources(this.removeToolStripMenuItem, "removeToolStripMenuItem");
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeDeviceToolStripButton_Click);
            // 
            // toolStripMenuItem1
            // 
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            // 
            // uploadToolStripMenuItem
            // 
            resources.ApplyResources(this.uploadToolStripMenuItem, "uploadToolStripMenuItem");
            this.uploadToolStripMenuItem.Name = "uploadToolStripMenuItem";
            this.uploadToolStripMenuItem.Click += new System.EventHandler(this.uploadToolStripButton_Click);
            // 
            // toolStripMenuItem2
            // 
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            // 
            // openToolStripMenuItem
            // 
            resources.ApplyResources(this.openToolStripMenuItem, "openToolStripMenuItem");
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripMenuItem
            // 
            resources.ApplyResources(this.saveToolStripMenuItem, "saveToolStripMenuItem");
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            resources.ApplyResources(this.saveAsToolStripMenuItem, "saveAsToolStripMenuItem");
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            // 
            // toolStripMenuItem3
            // 
            resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            // 
            // updateFirmwareToolStripMenuItem
            // 
            resources.ApplyResources(this.updateFirmwareToolStripMenuItem, "updateFirmwareToolStripMenuItem");
            this.updateFirmwareToolStripMenuItem.Name = "updateFirmwareToolStripMenuItem";
            this.updateFirmwareToolStripMenuItem.Click += new System.EventHandler(this.updateFirmwareToolStripMenuItem_Click);
            // 
            // regenerateSerialToolStripMenuItem
            // 
            resources.ApplyResources(this.regenerateSerialToolStripMenuItem, "regenerateSerialToolStripMenuItem");
            this.regenerateSerialToolStripMenuItem.Name = "regenerateSerialToolStripMenuItem";
            this.regenerateSerialToolStripMenuItem.Click += new System.EventHandler(this.regenerateSerialToolStripMenuItem_Click);
            // 
            // reloadConfigToolStripMenuItem
            // 
            resources.ApplyResources(this.reloadConfigToolStripMenuItem, "reloadConfigToolStripMenuItem");
            this.reloadConfigToolStripMenuItem.Name = "reloadConfigToolStripMenuItem";
            this.reloadConfigToolStripMenuItem.Click += new System.EventHandler(this.reloadConfigToolStripMenuItem_Click);
            // 
            // generalTabPage
            // 
            resources.ApplyResources(this.generalTabPage, "generalTabPage");
            this.generalTabPage.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.generalTabPage.Controls.Add(this.offlineModeGroupBox);
            this.generalTabPage.Controls.Add(this.debugGroupBox);
            this.generalTabPage.Controls.Add(this.testModeSpeedGroupBox);
            this.generalTabPage.Controls.Add(this.recentFilesGroupBox);
            this.errorProvider1.SetError(this.generalTabPage, resources.GetString("generalTabPage.Error"));
            this.errorProvider1.SetIconAlignment(this.generalTabPage, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("generalTabPage.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.generalTabPage, ((int)(resources.GetObject("generalTabPage.IconPadding"))));
            this.generalTabPage.Name = "generalTabPage";
            this.toolTip1.SetToolTip(this.generalTabPage, resources.GetString("generalTabPage.ToolTip"));
            // 
            // offlineModeGroupBox
            // 
            resources.ApplyResources(this.offlineModeGroupBox, "offlineModeGroupBox");
            this.offlineModeGroupBox.Controls.Add(this.label7);
            this.offlineModeGroupBox.Controls.Add(this.offlineModeCheckBox);
            this.errorProvider1.SetError(this.offlineModeGroupBox, resources.GetString("offlineModeGroupBox.Error"));
            this.errorProvider1.SetIconAlignment(this.offlineModeGroupBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("offlineModeGroupBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.offlineModeGroupBox, ((int)(resources.GetObject("offlineModeGroupBox.IconPadding"))));
            this.offlineModeGroupBox.Name = "offlineModeGroupBox";
            this.offlineModeGroupBox.TabStop = false;
            this.toolTip1.SetToolTip(this.offlineModeGroupBox, resources.GetString("offlineModeGroupBox.ToolTip"));
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.errorProvider1.SetError(this.label7, resources.GetString("label7.Error"));
            this.errorProvider1.SetIconAlignment(this.label7, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label7.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.label7, ((int)(resources.GetObject("label7.IconPadding"))));
            this.label7.Name = "label7";
            this.toolTip1.SetToolTip(this.label7, resources.GetString("label7.ToolTip"));
            // 
            // offlineModeCheckBox
            // 
            resources.ApplyResources(this.offlineModeCheckBox, "offlineModeCheckBox");
            this.errorProvider1.SetError(this.offlineModeCheckBox, resources.GetString("offlineModeCheckBox.Error"));
            this.errorProvider1.SetIconAlignment(this.offlineModeCheckBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("offlineModeCheckBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.offlineModeCheckBox, ((int)(resources.GetObject("offlineModeCheckBox.IconPadding"))));
            this.offlineModeCheckBox.Name = "offlineModeCheckBox";
            this.toolTip1.SetToolTip(this.offlineModeCheckBox, resources.GetString("offlineModeCheckBox.ToolTip"));
            this.offlineModeCheckBox.UseVisualStyleBackColor = true;
            // 
            // debugGroupBox
            // 
            resources.ApplyResources(this.debugGroupBox, "debugGroupBox");
            this.debugGroupBox.Controls.Add(this.logLevelComboBox);
            this.debugGroupBox.Controls.Add(this.logLevelLabel);
            this.debugGroupBox.Controls.Add(this.logLevelCheckBox);
            this.errorProvider1.SetError(this.debugGroupBox, resources.GetString("debugGroupBox.Error"));
            this.errorProvider1.SetIconAlignment(this.debugGroupBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("debugGroupBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.debugGroupBox, ((int)(resources.GetObject("debugGroupBox.IconPadding"))));
            this.debugGroupBox.Name = "debugGroupBox";
            this.debugGroupBox.TabStop = false;
            this.toolTip1.SetToolTip(this.debugGroupBox, resources.GetString("debugGroupBox.ToolTip"));
            // 
            // logLevelComboBox
            // 
            resources.ApplyResources(this.logLevelComboBox, "logLevelComboBox");
            this.logLevelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.errorProvider1.SetError(this.logLevelComboBox, resources.GetString("logLevelComboBox.Error"));
            this.logLevelComboBox.FormattingEnabled = true;
            this.errorProvider1.SetIconAlignment(this.logLevelComboBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("logLevelComboBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.logLevelComboBox, ((int)(resources.GetObject("logLevelComboBox.IconPadding"))));
            this.logLevelComboBox.Items.AddRange(new object[] {
            resources.GetString("logLevelComboBox.Items"),
            resources.GetString("logLevelComboBox.Items1"),
            resources.GetString("logLevelComboBox.Items2"),
            resources.GetString("logLevelComboBox.Items3")});
            this.logLevelComboBox.Name = "logLevelComboBox";
            this.toolTip1.SetToolTip(this.logLevelComboBox, resources.GetString("logLevelComboBox.ToolTip"));
            // 
            // logLevelLabel
            // 
            resources.ApplyResources(this.logLevelLabel, "logLevelLabel");
            this.errorProvider1.SetError(this.logLevelLabel, resources.GetString("logLevelLabel.Error"));
            this.errorProvider1.SetIconAlignment(this.logLevelLabel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("logLevelLabel.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.logLevelLabel, ((int)(resources.GetObject("logLevelLabel.IconPadding"))));
            this.logLevelLabel.Name = "logLevelLabel";
            this.toolTip1.SetToolTip(this.logLevelLabel, resources.GetString("logLevelLabel.ToolTip"));
            // 
            // logLevelCheckBox
            // 
            resources.ApplyResources(this.logLevelCheckBox, "logLevelCheckBox");
            this.errorProvider1.SetError(this.logLevelCheckBox, resources.GetString("logLevelCheckBox.Error"));
            this.errorProvider1.SetIconAlignment(this.logLevelCheckBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("logLevelCheckBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.logLevelCheckBox, ((int)(resources.GetObject("logLevelCheckBox.IconPadding"))));
            this.logLevelCheckBox.Name = "logLevelCheckBox";
            this.toolTip1.SetToolTip(this.logLevelCheckBox, resources.GetString("logLevelCheckBox.ToolTip"));
            this.logLevelCheckBox.UseVisualStyleBackColor = true;
            // 
            // testModeSpeedGroupBox
            // 
            resources.ApplyResources(this.testModeSpeedGroupBox, "testModeSpeedGroupBox");
            this.testModeSpeedGroupBox.Controls.Add(this.label8);
            this.testModeSpeedGroupBox.Controls.Add(this.label6);
            this.testModeSpeedGroupBox.Controls.Add(this.testModeSpeedTrackBar);
            this.errorProvider1.SetError(this.testModeSpeedGroupBox, resources.GetString("testModeSpeedGroupBox.Error"));
            this.errorProvider1.SetIconAlignment(this.testModeSpeedGroupBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("testModeSpeedGroupBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.testModeSpeedGroupBox, ((int)(resources.GetObject("testModeSpeedGroupBox.IconPadding"))));
            this.testModeSpeedGroupBox.Name = "testModeSpeedGroupBox";
            this.testModeSpeedGroupBox.TabStop = false;
            this.toolTip1.SetToolTip(this.testModeSpeedGroupBox, resources.GetString("testModeSpeedGroupBox.ToolTip"));
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.errorProvider1.SetError(this.label8, resources.GetString("label8.Error"));
            this.errorProvider1.SetIconAlignment(this.label8, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label8.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.label8, ((int)(resources.GetObject("label8.IconPadding"))));
            this.label8.Name = "label8";
            this.toolTip1.SetToolTip(this.label8, resources.GetString("label8.ToolTip"));
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.errorProvider1.SetError(this.label6, resources.GetString("label6.Error"));
            this.errorProvider1.SetIconAlignment(this.label6, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label6.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.label6, ((int)(resources.GetObject("label6.IconPadding"))));
            this.label6.Name = "label6";
            this.toolTip1.SetToolTip(this.label6, resources.GetString("label6.ToolTip"));
            // 
            // testModeSpeedTrackBar
            // 
            resources.ApplyResources(this.testModeSpeedTrackBar, "testModeSpeedTrackBar");
            this.errorProvider1.SetError(this.testModeSpeedTrackBar, resources.GetString("testModeSpeedTrackBar.Error"));
            this.errorProvider1.SetIconAlignment(this.testModeSpeedTrackBar, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("testModeSpeedTrackBar.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.testModeSpeedTrackBar, ((int)(resources.GetObject("testModeSpeedTrackBar.IconPadding"))));
            this.testModeSpeedTrackBar.Maximum = 4;
            this.testModeSpeedTrackBar.Name = "testModeSpeedTrackBar";
            this.toolTip1.SetToolTip(this.testModeSpeedTrackBar, resources.GetString("testModeSpeedTrackBar.ToolTip"));
            // 
            // recentFilesGroupBox
            // 
            resources.ApplyResources(this.recentFilesGroupBox, "recentFilesGroupBox");
            this.recentFilesGroupBox.Controls.Add(this.label1);
            this.recentFilesGroupBox.Controls.Add(this.recentFilesNumericUpDown);
            this.errorProvider1.SetError(this.recentFilesGroupBox, resources.GetString("recentFilesGroupBox.Error"));
            this.errorProvider1.SetIconAlignment(this.recentFilesGroupBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("recentFilesGroupBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.recentFilesGroupBox, ((int)(resources.GetObject("recentFilesGroupBox.IconPadding"))));
            this.recentFilesGroupBox.Name = "recentFilesGroupBox";
            this.recentFilesGroupBox.TabStop = false;
            this.toolTip1.SetToolTip(this.recentFilesGroupBox, resources.GetString("recentFilesGroupBox.ToolTip"));
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.AutoEllipsis = true;
            this.errorProvider1.SetError(this.label1, resources.GetString("label1.Error"));
            this.errorProvider1.SetIconAlignment(this.label1, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label1.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.label1, ((int)(resources.GetObject("label1.IconPadding"))));
            this.label1.Name = "label1";
            this.toolTip1.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
            // 
            // recentFilesNumericUpDown
            // 
            resources.ApplyResources(this.recentFilesNumericUpDown, "recentFilesNumericUpDown");
            this.errorProvider1.SetError(this.recentFilesNumericUpDown, resources.GetString("recentFilesNumericUpDown.Error"));
            this.errorProvider1.SetIconAlignment(this.recentFilesNumericUpDown, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("recentFilesNumericUpDown.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.recentFilesNumericUpDown, ((int)(resources.GetObject("recentFilesNumericUpDown.IconPadding"))));
            this.recentFilesNumericUpDown.Name = "recentFilesNumericUpDown";
            this.toolTip1.SetToolTip(this.recentFilesNumericUpDown, resources.GetString("recentFilesNumericUpDown.ToolTip"));
            // 
            // tabControl1
            // 
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Controls.Add(this.generalTabPage);
            this.tabControl1.Controls.Add(this.ledDisplaysTabPage);
            this.tabControl1.Controls.Add(this.mobiFlightTabPage);
            this.tabControl1.Controls.Add(this.fsuipcTabPage);
            this.errorProvider1.SetError(this.tabControl1, resources.GetString("tabControl1.Error"));
            this.errorProvider1.SetIconAlignment(this.tabControl1, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("tabControl1.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.tabControl1, ((int)(resources.GetObject("tabControl1.IconPadding"))));
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.toolTip1.SetToolTip(this.tabControl1, resources.GetString("tabControl1.ToolTip"));
            // 
            // mobiFlightTabPage
            // 
            resources.ApplyResources(this.mobiFlightTabPage, "mobiFlightTabPage");
            this.mobiFlightTabPage.Controls.Add(this.mfConfiguredModulesGroupBox);
            this.mobiFlightTabPage.Controls.Add(this.firmwareSettingsGroupBox);
            this.mobiFlightTabPage.Controls.Add(this.mobiflightSettingsLabel);
            this.errorProvider1.SetError(this.mobiFlightTabPage, resources.GetString("mobiFlightTabPage.Error"));
            this.errorProvider1.SetIconAlignment(this.mobiFlightTabPage, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("mobiFlightTabPage.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.mobiFlightTabPage, ((int)(resources.GetObject("mobiFlightTabPage.IconPadding"))));
            this.mobiFlightTabPage.Name = "mobiFlightTabPage";
            this.toolTip1.SetToolTip(this.mobiFlightTabPage, resources.GetString("mobiFlightTabPage.ToolTip"));
            this.mobiFlightTabPage.UseVisualStyleBackColor = true;
            // 
            // mfConfiguredModulesGroupBox
            // 
            resources.ApplyResources(this.mfConfiguredModulesGroupBox, "mfConfiguredModulesGroupBox");
            this.mfConfiguredModulesGroupBox.Controls.Add(this.mfModulesTreeView);
            this.mfConfiguredModulesGroupBox.Controls.Add(this.mfSettingsPanel);
            this.mfConfiguredModulesGroupBox.Controls.Add(this.mobiflightSettingsToolStrip);
            this.errorProvider1.SetError(this.mfConfiguredModulesGroupBox, resources.GetString("mfConfiguredModulesGroupBox.Error"));
            this.errorProvider1.SetIconAlignment(this.mfConfiguredModulesGroupBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("mfConfiguredModulesGroupBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.mfConfiguredModulesGroupBox, ((int)(resources.GetObject("mfConfiguredModulesGroupBox.IconPadding"))));
            this.mfConfiguredModulesGroupBox.Name = "mfConfiguredModulesGroupBox";
            this.mfConfiguredModulesGroupBox.TabStop = false;
            this.toolTip1.SetToolTip(this.mfConfiguredModulesGroupBox, resources.GetString("mfConfiguredModulesGroupBox.ToolTip"));
            // 
            // mfModulesTreeView
            // 
            resources.ApplyResources(this.mfModulesTreeView, "mfModulesTreeView");
            this.mfModulesTreeView.ContextMenuStrip = this.mfModuleSettingsContextMenuStrip;
            this.errorProvider1.SetError(this.mfModulesTreeView, resources.GetString("mfModulesTreeView.Error"));
            this.errorProvider1.SetIconAlignment(this.mfModulesTreeView, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("mfModulesTreeView.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.mfModulesTreeView, ((int)(resources.GetObject("mfModulesTreeView.IconPadding"))));
            this.mfModulesTreeView.ImageList = this.mfTreeViewImageList;
            this.mfModulesTreeView.Name = "mfModulesTreeView";
            this.mfModulesTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            ((System.Windows.Forms.TreeNode)(resources.GetObject("mfModulesTreeView.Nodes"))),
            ((System.Windows.Forms.TreeNode)(resources.GetObject("mfModulesTreeView.Nodes1")))});
            this.mfModulesTreeView.ShowNodeToolTips = true;
            this.toolTip1.SetToolTip(this.mfModulesTreeView, resources.GetString("mfModulesTreeView.ToolTip"));
            this.mfModulesTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.mfModulesTreeView_AfterSelect);
            // 
            // mfSettingsPanel
            // 
            resources.ApplyResources(this.mfSettingsPanel, "mfSettingsPanel");
            this.errorProvider1.SetError(this.mfSettingsPanel, resources.GetString("mfSettingsPanel.Error"));
            this.errorProvider1.SetIconAlignment(this.mfSettingsPanel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("mfSettingsPanel.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.mfSettingsPanel, ((int)(resources.GetObject("mfSettingsPanel.IconPadding"))));
            this.mfSettingsPanel.Name = "mfSettingsPanel";
            this.toolTip1.SetToolTip(this.mfSettingsPanel, resources.GetString("mfSettingsPanel.ToolTip"));
            // 
            // mobiflightSettingsToolStrip
            // 
            resources.ApplyResources(this.mobiflightSettingsToolStrip, "mobiflightSettingsToolStrip");
            this.errorProvider1.SetError(this.mobiflightSettingsToolStrip, resources.GetString("mobiflightSettingsToolStrip.Error"));
            this.errorProvider1.SetIconAlignment(this.mobiflightSettingsToolStrip, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("mobiflightSettingsToolStrip.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.mobiflightSettingsToolStrip, ((int)(resources.GetObject("mobiflightSettingsToolStrip.IconPadding"))));
            this.mobiflightSettingsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uploadToolStripButton,
            this.toolStripSeparator1,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator2,
            this.addDeviceToolStripDropDownButton,
            this.removeDeviceToolStripButton,
            this.toolStripSeparator4});
            this.mobiflightSettingsToolStrip.Name = "mobiflightSettingsToolStrip";
            this.toolTip1.SetToolTip(this.mobiflightSettingsToolStrip, resources.GetString("mobiflightSettingsToolStrip.ToolTip"));
            this.mobiflightSettingsToolStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mobiflightSettingsToolStrip_ItemClicked);
            // 
            // uploadToolStripButton
            // 
            resources.ApplyResources(this.uploadToolStripButton, "uploadToolStripButton");
            this.uploadToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uploadToolStripButton.Image = global::MobiFlight.Properties.Resources.export1;
            this.uploadToolStripButton.Name = "uploadToolStripButton";
            this.uploadToolStripButton.Click += new System.EventHandler(this.uploadToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            // 
            // openToolStripButton
            // 
            resources.ApplyResources(this.openToolStripButton, "openToolStripButton");
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = global::MobiFlight.Properties.Resources.folder;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            resources.ApplyResources(this.saveToolStripButton, "saveToolStripButton");
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = global::MobiFlight.Properties.Resources.disk_blue;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            // 
            // addDeviceToolStripDropDownButton
            // 
            resources.ApplyResources(this.addDeviceToolStripDropDownButton, "addDeviceToolStripDropDownButton");
            this.addDeviceToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addEncoderToolStripMenuItem,
            this.addButtonToolStripMenuItem,
            this.toolStripSeparator3,
            this.addStepperToolStripMenuItem,
            this.addServoToolStripMenuItem,
            this.addLedModuleToolStripMenuItem,
            this.addOutputToolStripMenuItem,
            this.addLcdDisplayToolStripMenuItem});
            this.addDeviceToolStripDropDownButton.Image = global::MobiFlight.Properties.Resources.star_yellow_add;
            this.addDeviceToolStripDropDownButton.Name = "addDeviceToolStripDropDownButton";
            // 
            // addEncoderToolStripMenuItem
            // 
            resources.ApplyResources(this.addEncoderToolStripMenuItem, "addEncoderToolStripMenuItem");
            this.addEncoderToolStripMenuItem.Name = "addEncoderToolStripMenuItem";
            this.addEncoderToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addButtonToolStripMenuItem
            // 
            resources.ApplyResources(this.addButtonToolStripMenuItem, "addButtonToolStripMenuItem");
            this.addButtonToolStripMenuItem.Name = "addButtonToolStripMenuItem";
            this.addButtonToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            // 
            // addStepperToolStripMenuItem
            // 
            resources.ApplyResources(this.addStepperToolStripMenuItem, "addStepperToolStripMenuItem");
            this.addStepperToolStripMenuItem.Name = "addStepperToolStripMenuItem";
            this.addStepperToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addServoToolStripMenuItem
            // 
            resources.ApplyResources(this.addServoToolStripMenuItem, "addServoToolStripMenuItem");
            this.addServoToolStripMenuItem.Name = "addServoToolStripMenuItem";
            this.addServoToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addLedModuleToolStripMenuItem
            // 
            resources.ApplyResources(this.addLedModuleToolStripMenuItem, "addLedModuleToolStripMenuItem");
            this.addLedModuleToolStripMenuItem.Name = "addLedModuleToolStripMenuItem";
            this.addLedModuleToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addOutputToolStripMenuItem
            // 
            resources.ApplyResources(this.addOutputToolStripMenuItem, "addOutputToolStripMenuItem");
            this.addOutputToolStripMenuItem.Name = "addOutputToolStripMenuItem";
            this.addOutputToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // addLcdDisplayToolStripMenuItem
            // 
            resources.ApplyResources(this.addLcdDisplayToolStripMenuItem, "addLcdDisplayToolStripMenuItem");
            this.addLcdDisplayToolStripMenuItem.Name = "addLcdDisplayToolStripMenuItem";
            this.addLcdDisplayToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeToolStripMenuItem_Click);
            // 
            // removeDeviceToolStripButton
            // 
            resources.ApplyResources(this.removeDeviceToolStripButton, "removeDeviceToolStripButton");
            this.removeDeviceToolStripButton.Image = global::MobiFlight.Properties.Resources.star_yellow_delete;
            this.removeDeviceToolStripButton.Name = "removeDeviceToolStripButton";
            this.removeDeviceToolStripButton.Click += new System.EventHandler(this.removeDeviceToolStripButton_Click);
            // 
            // toolStripSeparator4
            // 
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            // 
            // firmwareSettingsGroupBox
            // 
            resources.ApplyResources(this.firmwareSettingsGroupBox, "firmwareSettingsGroupBox");
            this.firmwareSettingsGroupBox.Controls.Add(this.FwAutoUpdateCheckBox);
            this.firmwareSettingsGroupBox.Controls.Add(this.firmwareArduinoIdeButton);
            this.firmwareSettingsGroupBox.Controls.Add(this.firmwareArduinoIdePathTextBox);
            this.firmwareSettingsGroupBox.Controls.Add(this.firmwareArduinoIdeLabel);
            this.errorProvider1.SetError(this.firmwareSettingsGroupBox, resources.GetString("firmwareSettingsGroupBox.Error"));
            this.errorProvider1.SetIconAlignment(this.firmwareSettingsGroupBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("firmwareSettingsGroupBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.firmwareSettingsGroupBox, ((int)(resources.GetObject("firmwareSettingsGroupBox.IconPadding"))));
            this.firmwareSettingsGroupBox.Name = "firmwareSettingsGroupBox";
            this.firmwareSettingsGroupBox.TabStop = false;
            this.toolTip1.SetToolTip(this.firmwareSettingsGroupBox, resources.GetString("firmwareSettingsGroupBox.ToolTip"));
            // 
            // FwAutoUpdateCheckBox
            // 
            resources.ApplyResources(this.FwAutoUpdateCheckBox, "FwAutoUpdateCheckBox");
            this.errorProvider1.SetError(this.FwAutoUpdateCheckBox, resources.GetString("FwAutoUpdateCheckBox.Error"));
            this.errorProvider1.SetIconAlignment(this.FwAutoUpdateCheckBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("FwAutoUpdateCheckBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.FwAutoUpdateCheckBox, ((int)(resources.GetObject("FwAutoUpdateCheckBox.IconPadding"))));
            this.FwAutoUpdateCheckBox.Name = "FwAutoUpdateCheckBox";
            this.toolTip1.SetToolTip(this.FwAutoUpdateCheckBox, resources.GetString("FwAutoUpdateCheckBox.ToolTip"));
            this.FwAutoUpdateCheckBox.UseVisualStyleBackColor = true;
            // 
            // firmwareArduinoIdeButton
            // 
            resources.ApplyResources(this.firmwareArduinoIdeButton, "firmwareArduinoIdeButton");
            this.errorProvider1.SetError(this.firmwareArduinoIdeButton, resources.GetString("firmwareArduinoIdeButton.Error"));
            this.errorProvider1.SetIconAlignment(this.firmwareArduinoIdeButton, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("firmwareArduinoIdeButton.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.firmwareArduinoIdeButton, ((int)(resources.GetObject("firmwareArduinoIdeButton.IconPadding"))));
            this.firmwareArduinoIdeButton.Image = global::MobiFlight.Properties.Resources.folder1;
            this.firmwareArduinoIdeButton.Name = "firmwareArduinoIdeButton";
            this.toolTip1.SetToolTip(this.firmwareArduinoIdeButton, resources.GetString("firmwareArduinoIdeButton.ToolTip"));
            this.firmwareArduinoIdeButton.UseVisualStyleBackColor = true;
            this.firmwareArduinoIdeButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // firmwareArduinoIdePathTextBox
            // 
            resources.ApplyResources(this.firmwareArduinoIdePathTextBox, "firmwareArduinoIdePathTextBox");
            this.errorProvider1.SetError(this.firmwareArduinoIdePathTextBox, resources.GetString("firmwareArduinoIdePathTextBox.Error"));
            this.errorProvider1.SetIconAlignment(this.firmwareArduinoIdePathTextBox, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("firmwareArduinoIdePathTextBox.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.firmwareArduinoIdePathTextBox, ((int)(resources.GetObject("firmwareArduinoIdePathTextBox.IconPadding"))));
            this.firmwareArduinoIdePathTextBox.Name = "firmwareArduinoIdePathTextBox";
            this.toolTip1.SetToolTip(this.firmwareArduinoIdePathTextBox, resources.GetString("firmwareArduinoIdePathTextBox.ToolTip"));
            this.firmwareArduinoIdePathTextBox.TextChanged += new System.EventHandler(this.firmwareArduinoIdePathTextBox_TextChanged);
            this.firmwareArduinoIdePathTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.firmwareArduinoIdePathTextBox_Validating);
            // 
            // firmwareArduinoIdeLabel
            // 
            resources.ApplyResources(this.firmwareArduinoIdeLabel, "firmwareArduinoIdeLabel");
            this.errorProvider1.SetError(this.firmwareArduinoIdeLabel, resources.GetString("firmwareArduinoIdeLabel.Error"));
            this.errorProvider1.SetIconAlignment(this.firmwareArduinoIdeLabel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("firmwareArduinoIdeLabel.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.firmwareArduinoIdeLabel, ((int)(resources.GetObject("firmwareArduinoIdeLabel.IconPadding"))));
            this.firmwareArduinoIdeLabel.Name = "firmwareArduinoIdeLabel";
            this.toolTip1.SetToolTip(this.firmwareArduinoIdeLabel, resources.GetString("firmwareArduinoIdeLabel.ToolTip"));
            // 
            // mobiflightSettingsLabel
            // 
            resources.ApplyResources(this.mobiflightSettingsLabel, "mobiflightSettingsLabel");
            this.errorProvider1.SetError(this.mobiflightSettingsLabel, resources.GetString("mobiflightSettingsLabel.Error"));
            this.errorProvider1.SetIconAlignment(this.mobiflightSettingsLabel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("mobiflightSettingsLabel.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.mobiflightSettingsLabel, ((int)(resources.GetObject("mobiflightSettingsLabel.IconPadding"))));
            this.mobiflightSettingsLabel.Name = "mobiflightSettingsLabel";
            this.toolTip1.SetToolTip(this.mobiflightSettingsLabel, resources.GetString("mobiflightSettingsLabel.ToolTip"));
            this.mobiflightSettingsLabel.Click += new System.EventHandler(this.mobiflightSettingsLabel_Click);
            // 
            // fsuipcTabPage
            // 
            resources.ApplyResources(this.fsuipcTabPage, "fsuipcTabPage");
            this.fsuipcTabPage.Controls.Add(this.groupBox1);
            this.errorProvider1.SetError(this.fsuipcTabPage, resources.GetString("fsuipcTabPage.Error"));
            this.errorProvider1.SetIconAlignment(this.fsuipcTabPage, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("fsuipcTabPage.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.fsuipcTabPage, ((int)(resources.GetObject("fsuipcTabPage.IconPadding"))));
            this.fsuipcTabPage.Name = "fsuipcTabPage";
            this.toolTip1.SetToolTip(this.fsuipcTabPage, resources.GetString("fsuipcTabPage.ToolTip"));
            this.fsuipcTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.fsuipcPollIntervalTrackBar);
            this.errorProvider1.SetError(this.groupBox1, resources.GetString("groupBox1.Error"));
            this.errorProvider1.SetIconAlignment(this.groupBox1, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("groupBox1.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.groupBox1, ((int)(resources.GetObject("groupBox1.IconPadding"))));
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.groupBox1, resources.GetString("groupBox1.ToolTip"));
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.errorProvider1.SetError(this.label4, resources.GetString("label4.Error"));
            this.errorProvider1.SetIconAlignment(this.label4, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label4.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.label4, ((int)(resources.GetObject("label4.IconPadding"))));
            this.label4.Name = "label4";
            this.toolTip1.SetToolTip(this.label4, resources.GetString("label4.ToolTip"));
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.errorProvider1.SetError(this.label3, resources.GetString("label3.Error"));
            this.errorProvider1.SetIconAlignment(this.label3, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label3.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.label3, ((int)(resources.GetObject("label3.IconPadding"))));
            this.label3.Name = "label3";
            this.toolTip1.SetToolTip(this.label3, resources.GetString("label3.ToolTip"));
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.errorProvider1.SetError(this.label2, resources.GetString("label2.Error"));
            this.errorProvider1.SetIconAlignment(this.label2, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label2.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.label2, ((int)(resources.GetObject("label2.IconPadding"))));
            this.label2.Name = "label2";
            this.toolTip1.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.errorProvider1.SetError(this.label5, resources.GetString("label5.Error"));
            this.errorProvider1.SetIconAlignment(this.label5, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label5.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.label5, ((int)(resources.GetObject("label5.IconPadding"))));
            this.label5.Name = "label5";
            this.toolTip1.SetToolTip(this.label5, resources.GetString("label5.ToolTip"));
            // 
            // fsuipcPollIntervalTrackBar
            // 
            resources.ApplyResources(this.fsuipcPollIntervalTrackBar, "fsuipcPollIntervalTrackBar");
            this.fsuipcPollIntervalTrackBar.BackColor = System.Drawing.SystemColors.Window;
            this.errorProvider1.SetError(this.fsuipcPollIntervalTrackBar, resources.GetString("fsuipcPollIntervalTrackBar.Error"));
            this.errorProvider1.SetIconAlignment(this.fsuipcPollIntervalTrackBar, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("fsuipcPollIntervalTrackBar.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.fsuipcPollIntervalTrackBar, ((int)(resources.GetObject("fsuipcPollIntervalTrackBar.IconPadding"))));
            this.fsuipcPollIntervalTrackBar.LargeChange = 2;
            this.fsuipcPollIntervalTrackBar.Minimum = 2;
            this.fsuipcPollIntervalTrackBar.Name = "fsuipcPollIntervalTrackBar";
            this.toolTip1.SetToolTip(this.fsuipcPollIntervalTrackBar, resources.GetString("fsuipcPollIntervalTrackBar.ToolTip"));
            this.fsuipcPollIntervalTrackBar.Value = 10;
            // 
            // firmwareSettingsToolStripMenuItem
            // 
            resources.ApplyResources(this.firmwareSettingsToolStripMenuItem, "firmwareSettingsToolStripMenuItem");
            this.firmwareSettingsToolStripMenuItem.Name = "firmwareSettingsToolStripMenuItem";
            // 
            // toolStripSeparator5
            // 
            resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            // 
            // firmwareUpdateToolStripMenuItem
            // 
            resources.ApplyResources(this.firmwareUpdateToolStripMenuItem, "firmwareUpdateToolStripMenuItem");
            this.firmwareUpdateToolStripMenuItem.Name = "firmwareUpdateToolStripMenuItem";
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip1.ToolTipTitle = "Hint";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            resources.ApplyResources(this.errorProvider1, "errorProvider1");
            // 
            // SettingsDialog
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SettingsDialog";
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.Shown += new System.EventHandler(this.SettingsDialog_Shown);
            this.panel1.ResumeLayout(false);
            this.ledDisplaysTabPage.ResumeLayout(false);
            this.arcazeModuleSettingsGroupBox.ResumeLayout(false);
            this.arcazeModuleSettingsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numModulesNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.globalBrightnessTrackBar)).EndInit();
            this.arcazeModulesGroupBox.ResumeLayout(false);
            this.mfModuleSettingsContextMenuStrip.ResumeLayout(false);
            this.generalTabPage.ResumeLayout(false);
            this.offlineModeGroupBox.ResumeLayout(false);
            this.offlineModeGroupBox.PerformLayout();
            this.debugGroupBox.ResumeLayout(false);
            this.debugGroupBox.PerformLayout();
            this.testModeSpeedGroupBox.ResumeLayout(false);
            this.testModeSpeedGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testModeSpeedTrackBar)).EndInit();
            this.recentFilesGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recentFilesNumericUpDown)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.mobiFlightTabPage.ResumeLayout(false);
            this.mfConfiguredModulesGroupBox.ResumeLayout(false);
            this.mfConfiguredModulesGroupBox.PerformLayout();
            this.mobiflightSettingsToolStrip.ResumeLayout(false);
            this.mobiflightSettingsToolStrip.PerformLayout();
            this.firmwareSettingsGroupBox.ResumeLayout(false);
            this.firmwareSettingsGroupBox.PerformLayout();
            this.fsuipcTabPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fsuipcPollIntervalTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.TabPage ledDisplaysTabPage;
        private System.Windows.Forms.GroupBox arcazeModulesGroupBox;
        private System.Windows.Forms.TabPage generalTabPage;
        private System.Windows.Forms.GroupBox recentFilesGroupBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown recentFilesNumericUpDown;
        private System.Windows.Forms.TabPage fsuipcTabPage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar fsuipcPollIntervalTrackBar;
        private System.Windows.Forms.GroupBox arcazeModuleSettingsGroupBox;
        private System.Windows.Forms.Label numModulesLabel;
        private System.Windows.Forms.NumericUpDown numModulesNumericUpDown;
        private System.Windows.Forms.ComboBox arcazeModuleTypeComboBox;
        private System.Windows.Forms.Label globalBrightnessLabel;
        private System.Windows.Forms.TrackBar globalBrightnessTrackBar;
        private System.Windows.Forms.Label arcazeModuleTypeLabel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox testModeSpeedGroupBox;
        private System.Windows.Forms.TrackBar testModeSpeedTrackBar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox mfConfiguredModulesGroupBox;
        private System.Windows.Forms.Panel mfSettingsPanel;
        private System.Windows.Forms.TreeView mfModulesTreeView;
        private System.Windows.Forms.Label mobiflightSettingsLabel;
        private System.Windows.Forms.ContextMenuStrip mfModuleSettingsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ledOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ledSegmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stepperToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem uploadToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStrip mobiflightSettingsToolStrip;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton uploadToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton removeDeviceToolStripButton;
        private System.Windows.Forms.ToolStripDropDownButton addDeviceToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem addStepperToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addServoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addLedModuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addOutputToolStripMenuItem;
        private System.Windows.Forms.ImageList mfTreeViewImageList;
        private System.Windows.Forms.ToolStripMenuItem addEncoderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addButtonToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.GroupBox debugGroupBox;
        private System.Windows.Forms.ComboBox logLevelComboBox;
        private System.Windows.Forms.Label logLevelLabel;
        private System.Windows.Forms.CheckBox logLevelCheckBox;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem updateFirmwareToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem firmwareSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem firmwareUpdateToolStripMenuItem;
        private System.Windows.Forms.GroupBox firmwareSettingsGroupBox;
        private System.Windows.Forms.Button firmwareArduinoIdeButton;
        private System.Windows.Forms.TextBox firmwareArduinoIdePathTextBox;
        private System.Windows.Forms.Label firmwareArduinoIdeLabel;
        private System.ComponentModel.BackgroundWorker firmwareUpdateBackgroundWorker;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem buttonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem encoderToolStripMenuItem;
        public System.Windows.Forms.TabControl tabControl1;
        public System.Windows.Forms.TabPage mobiFlightTabPage;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ToolStripMenuItem regenerateSerialToolStripMenuItem;
        private System.Windows.Forms.TreeView ArcazeModuleTreeView;
        private System.Windows.Forms.Label arcazeSettingsLabel;
        private System.Windows.Forms.CheckBox FwAutoUpdateCheckBox;
        private System.Windows.Forms.ToolStripMenuItem reloadConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LcdDisplayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addLcdDisplayToolStripMenuItem;
        private System.Windows.Forms.GroupBox offlineModeGroupBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox offlineModeCheckBox;
    }
}